package main

import (
	"fmt"
	"github.com/json-iterator/go"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type vulnerabilitySummary struct {
	Name       string `json:"name"`
	Cve        string `json:"cve"`
	Level      string `json:"level"`
	Cpe        string `json:"cpe"`
	VersionFix string `json:"version_fix"`
}

func extractVulnerabilities(data []byte) []vulnerabilitySummary {
	var dependenciesCount = jsoniter.Get(data, "dependencies").Size()
	var vulnCount int
	var vulnerableSoftwareCount int
	var currentVulnerability vulnerabilitySummary
	var listVulnerabilities = make([]vulnerabilitySummary, 0)

	for i := 0; i < dependenciesCount; i++ {
		currentVulnerability = vulnerabilitySummary{Name: jsoniter.Get(data, "dependencies", i).Get("fileName").ToString()}
		if jsoniter.Get(data, "dependencies", i).Get("vulnerabilities").GetInterface() != nil {
			vulnCount = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities").Size()
			for v := 0; v < vulnCount; v++ {
				currentVulnerability.Cve = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("name").ToString()
				currentVulnerability.Level = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("severity").ToString()
				vulnerableSoftwareCount = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("vulnerableSoftware").Size()
				for vs := 0; vs < vulnerableSoftwareCount; vs++ {
					if jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("vulnerableSoftware", vs).Get("software").Get("vulnerabilityIdMatched").ToString() == "true" {
						currentVulnerability.Cpe = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("vulnerableSoftware", vs).Get("software").Get("id").ToString()
						currentVulnerability.VersionFix = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("vulnerableSoftware", vs).Get("software").Get("versionEndExcluding").ToString()
						listVulnerabilities = append(listVulnerabilities, currentVulnerability)
						//fmt.Println(currentVulnerability)
					}
				}
			}

		}
	}
	return listVulnerabilities
}

func main() {

	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) > 0 {
		err := filepath.Walk(argsWithoutProg[0],
			func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if strings.HasSuffix(path, ".json") {
					data, err := ioutil.ReadFile(path)
					if err != nil {
						log.Println(err)
					}
					var result = extractVulnerabilities(data)
					fmt.Println(path)
					for i := 0; i < len(result); i++ {
						fmt.Println(result[i])
					}
				}
				return nil
			})
		if err != nil {
			log.Println(err)
		}
	} else {
		log.Println("One parameter required: Folder to browse")
	}

}
