FROM golang:alpine as build

LABEL maintainer="SecuDev <remy.chaix@gmail.com>"

WORKDIR $GOPATH/src/depCheckReader

COPY DepCheckReader.go .

RUN apk add --no-cache git mercurial \
 && go get -d -v ./... \
 && go install -v ./... \
 && apk del git mercurial

FROM alpine:latest

COPY --from=build /go/bin/depCheckReader /usr/local/bin/depCheckReader

ENTRYPOINT ["/usr/local/bin/depCheckReader"]